# 投票
---

[TOC]

## 登录

### 用户登录
post:
```
/api/user/login
```
body: 
```typescript
{"username": string, "password": string}
```
res: 
```typescript
{w
    "code": number,
    "message": string
}
```

### 退出
get:
```
/api/user/logout
```
> * 退出只需要设置cookie的expires过期时间为0即可


## 候选人信息

### 获取候选人列表

> * 需要检查登录 checkLogin

post:
```
/api/person/list
```

body:
```typescript
{
    type: string
}
```

res:
```typescript
{
    "code": number,
    "message": string,
    "data": Array
}
```

### 获取候选人详情

> * 需要检查登录 checkLogin

> * 注意： 详情的data返回的是一个对象，不是数组
> 
post:
```
/api/person/detail
```

body: 
```typescript
{ _id: string }
```

res: 
```typescript
{
    "code": number,
    "message": string,
    "data": Object
}
```

## 投票

> * 需要检查登录 checkLogin

post:
```
/api/vote
```

body: 
```typescript
{

}
```

res:
```
```

## 投票情况列表

get:
```
/api/info/list
```
body:
```typescript
{}
```
res:
```typescript

```

## 附录
### 成功接口
```typescript
{
    "code": 0,
    "message": string,
    "data": any
}
```
### 失败接口
```typescript
{
    "code": -1,
    "message": string,
    "data": any
}
```
### 检查登录 checkLogin
```typescript
{
    code: -1,
    message: "尚未登录"
}
```