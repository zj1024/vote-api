class BaseModel {
  /**
   * 返回数据格式
   * @param {*} data 
   * @param {string} message 
   * @param {object} params 附加参数，比如文章数据总长度
   */
  constructor (data, message, params) {
    if (typeof data === 'string') {
      this.message = data
      data = null
      message = null
    }
    if (data) {
      this.data = data
    }
    if (message) {
      this.message = message
    }
    if (params) {
      this.count = params.count
    }
  }
}

class SuccessModel extends BaseModel {
  constructor(data, message, options) {
    super(data, message, options)
    this.code = 0
  }
}

class ErrorModel extends BaseModel {
  constructor(data, message) {
    super(data, message)
    this.code = -1
  }
}

module.exports = {
  SuccessModel,
  ErrorModel
}
