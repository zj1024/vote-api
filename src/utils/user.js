const { ErrorModel } = require('../model/resModel')

/**
 * 检查登录
 * @param {string} username 
 */
const checkLogin = (username) => {
  if (!username) {
    return new ErrorModel('尚未登录')
  }
}
/**
 * 检查用户权限
 * @param {string} role 
 */
const checkAuthor = (role) => {
  if (role !== 'admin') {
    return new ErrorModel('没有权限')
  }
}

module.exports = {
  checkLogin,
  checkAuthor
}
