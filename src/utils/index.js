const getExpires = require('./getExpires')
const getPostData = require('./getPostData')
const log = require('./log')
const { trim } = require('./string')
const { access } = require('./log')
const { checkLogin, checkAuthor } = require('./user')

module.exports = {
  getExpires,
  getPostData,
  log,
  trim,
  access,
  checkLogin,
  checkAuthor
}