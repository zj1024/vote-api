// 获取当前GMT时间
module.exports = () => {
  const D = new Date()
  D.setTime(D.getTime() + 60 * 1000 * 60 * 24)
  return D.toGMTString()
}
