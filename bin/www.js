const http = require('http')
const serverHandle = require('../app')

const PORT = 6666
const server = http.createServer(serverHandle)

server.listen(PORT, () => {
  console.log(`Server Listen on http://localhost:${PORT}`)
})
