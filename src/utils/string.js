/**
 * 去除两边空格
 * @param {string} str 
 */
const trim = (str) => {
  if (typeof str !== 'string') throw new Error('Type is not string')
  return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
}
module.exports = {
  trim
}
