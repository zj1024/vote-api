const { exec, escape } = require('../db/mysql')
const TABLE = 'tp_user'
const VOTER = 'tp_voter'

/**
 * 登录
 * @param {string} account 
 * @param {string} password
 */
const login = async (account, password) => {
  account = escape(account), password = escape(password)
  const sql = `select * from ${TABLE} where account=${account} and password=${password}`
  return await exec(sql)
}

const getVoterStatus = async (account) => {
  account = escape(account)
  const sql = `select status from ${VOTER} where account=${account}`
  return await exec(sql)
}

module.exports = {
  login,
  getVoterStatus
}
