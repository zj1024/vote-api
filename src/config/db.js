const env = process.env.NODE_ENV || 'dev'

let MYSQL_CONF, REDIS_CONF

// 开发环境
if (env === 'dev') {
  MYSQL_CONF = {
    host: 'localhost',
    user: 'root',
    password: 'zj1998++',
    port: '3306',
    database: 'vote'
  }

  REDIS_CONF = {
    host: 'localhost',
    port: '6379'
  }
}

// 生产环境
if (env === 'production') {
  MYSQL_CONF = {
    host: 'localhost',
    user: 'root',
    password: 'zj1998++',
    port: '3306',
    database: 'vote'
  }

  REDIS_CONF = {
    host: 'localhost',
    port: '6379'
  }
}

module.exports = {
  MYSQL_CONF,
  REDIS_CONF
}
