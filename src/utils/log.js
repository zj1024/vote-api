const fs = require('fs')
const path = require('path')
/**
 * 生产环境写日志
 * @param {string} log 
 */
const access = (log) => {
  const createWriteStream = (filename) => {
    const fullFileName = path.resolve(__dirname, '../', '../logs', filename)
    return fs.createWriteStream(fullFileName, {flags: 'a'})
  }
  const accessStream = createWriteStream('access.log')
  accessStream.write(`${log}\n`)
}
module.exports = {
  access
}
