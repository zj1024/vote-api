module.exports = async (req, res) => {
  const host = req.headers['host']
  return {
    'host': `${host}`,
  }
}
